## 优化修改说明

* 基于最新版修改，主文件不加入插件，插件任然按需加载
* 图片上传优化,只可选择图片类型
* 图片大小限制
* 图片分辨率大小限制
* 图片上传前压缩
* 多媒体上传支持mp4
* 多媒体上传支持大小限制
* 附件文件上传支持大小限制
* 多图片上传支持html5上传,支持压缩后上传
* 去掉flash上传功能,采用webUploader上传多图
* 美化弹层UI，按钮不再是硬邦邦的了
* 替换更可爱的表情
* 选择文件后自动上传改为点击按钮才上传
* 更多说明：https://gitee.com/aget/kindeditor

## 使用方式
* 快速使用（可以在服务环境下访问index.html页面测试参考）
```
//引入kindeditor-src.js文件
var editor = K.create('#content', {
    minHeight: 380,
    allowMediaUpload: true,
    allowMediaSize: 50,         //限制媒体上传50M
    allowFileManager: false,
    allowMediaSize: 10,         //限制视频文件上传10M
    allowFileSize: 10,          //限制文件上传10M
    allowImageSize: 5,          //限制图片上传5M
    maxWidth:900,               //压缩多少宽度后再上传
    maxHeight:1200,
    uploadJson: './php/testdata.php?act=upload',     //上传接口
    filePostName: 'file',
    afterUpload: function (url,res,uploadType) {
        console.log(uploadType,res);
    }           
});

```
* 接口数据返回格式
```
{
    code:1,   //1成功,否则失败
    data: {url:'上传成功的文件地址'},
    msg:'提示信息'
}
```

* 文件管理器返回格式
```
{
    code:1,   //1成功,否则失败
    data: {
        "parentdir":"",
        "dirpath":"",
        "dirurl":"/upload/xxx/",
        "count":4,
        "files":[ {
            "is_dir":true,
            "has_file":true,
            "filesize":0,
            "is_photo":false,
            "filetype":"",
            "filename":"2309i",
            "datetime":"2023-09-22 21:00:02"
        },
    ],
    msg:'提示信息'
}
```

## What is KindEditor?

KindEditor is a lightweight, Open Source(LGPL), cross browser, web based WYSIWYG HTML editor. KindEditor has the ability to convert standard textareas to rich text editing.

## Official site

http://kindeditor.org/

## Contributors

* Timon Lin
* daif alotaibi (http://daif.net/) : Arabic Translation
* fisker (https://github.com/fisker) : QQ style theme
* composite (https://github.com/composite) : Korean Translation

